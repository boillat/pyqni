# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 18:46:23 2020

@author: boillat
"""

from PyQNI import compound, scintillator

_gadox = compound({'Gd':2.0, 'O':2.0, 'S':1.0}, spec_weight=7.2).add_porosity(0.4)
gadox_sc = scintillator([_gadox],  capt_element='Gd')

_gadox157 = compound({'Gd-157':2.0, 'O':2.0, 'S':1.0}, spec_weight=7.2).add_porosity(0.4)
gadox157_sc = scintillator([_gadox157],  capt_element='Gd-157')

_lif = compound({'Li-6':1, 'F':1}, spec_weight=2.64).add_porosity(0.76)
lif_sc = scintillator([_lif],  capt_element='Li-6')
