# -*- coding: utf-8 -*-
"""
Created on Mon Dec 23 19:36:59 2019

@author: boillat
"""

from PyQNI_elements import element_list as el_list 
import numpy as np
    
    # List of authorized component fraction types
frac_type_list = ['mole', 'mass', 'vol']

    # Default set of neutron energies
def_evals = 10**(np.arange(-4,1,0.01))

    # Directories definitions
xs_dir = r'.\xs_data\xs_special' # Special cross sections
endf_dir = r'.\xs_data\ENDF_VIII' # ENDF VIII cross sections
spectra_dir = r'spectra_data' # Beamlines spectra

    # Constants
energy_1A = 0.08181 # Energy of neutrons at 1 Angstroem
Na = 6.02e23 # Avogadro number

"""----------------------------------------------------------------------------

                                    base
                                    
-------------------------------------------------------------------------------
                                    
Base class for all PyQNI objects, only define utility functions.
                                    
-------------------------------------------------------------------------------
          
Public methods:         
    None   
        
----------------------------------------------------------------------------"""

class base():
    
    """-------------------- _resolve_value----------------------------------"""
    
    def _resolve_value(self, value, **kwargs):
        """
                        
        Internal function used to resolve a parameter value
        - If the parameter is a float or an integer, its value is returned
        - If the parameter is a string, the value of the parameter defined by
          this string is returned
        - If the parameter is a function, it is evaluted and the result is returned
        This function is used in child classes to replace the value of parameters
        defined by strings or functions. For example, the stoichiometry of a
        given atom can be specified as the string 'x' instead of a numeric value.
        When cross sections or other parameters are evaluated, this stoichiometry
        will be replaced by the value of the parameter 'x' passed to the evaluation
        function.

        Parameters
        ----------
        value : float, int str or function
            Parameter to evaluate.
        **kwargs : dictionnary
            All other named parameters.

        Raises
        ------
        ValueError
            If the parameter type is not in the allowed list (float, int, str, function).

        Returns
        -------
        retval : float or int
            Resolved value of the parameter.

        """
        
        import numbers
            
            # if the parameter is a float or an integer (or None), just return it
        if isinstance(value, numbers.Number) or value is None:
            retval = value
        
            # if the parameter is a string
        elif isinstance(value, str):
            
                # if the string corresponds to a named parameter, return its value
            if value in kwargs.keys():
                retval = kwargs[value]
                
                # otherwise evaluate the string (developing the named parameters
                # to local variables)
            else:
                _l = locals()
                for kw in kwargs.keys():
                    _l[kw] = kwargs[kw]
                retval = eval(value)
                
            # if the parameter is a function, evaluate it
        elif callable(value):
            retval = value(**kwargs)
            
            # if the parameter is non of the above, raise an error 
        else:
            raise ValueError('Cannot convert ' + str(value) + ' to a number (type is "' + str(type(value)) + '"')
            
        return retval
    
    """---------------------- _atom_mass------------------------------------"""
    
    def _atom_mass(self, element, nat_mass=False):
        """
        Return the atomic mass of an element or isotope.

        Parameters
        ----------
        element : string
            Element or isotope definition (e.g. 'H' or 'Li-6')
        nat_mass : boolean
            If true, all calculation involving atomic masses use the mass of the
            natural isotopic composition, even for isotopically enriched compounds  

        Returns
        -------
        atom_mass : int or float
            Atomic mass in [g/mol].

        """
        
            # if the name is found in the elements list
        if element in el_list.keys():
            
                # create a compund with only this element and compute its molar mass
            atom_mass = compound(el_list[element]).molar_mass()
            
            # otherwise assume it is an isotope. If the 'nat_mass' option is set,
            # extract the element part (portion before the first hyphen)
            # and get its mass for the natural composition
        elif nat_mass:
            
            atom_mass = compound(el_list[element.split('-')[0]]).molar_mass()
        
            # if nat_mass is not set, get the real atomic mass of the isotope
            # from the portion after the first hyphen
        else:
            atom_mass = int(element.split('-')[1])
            
            # return the value
        return atom_mass
        

"""----------------------------------------------------------------------------

                                    material
                                    
-------------------------------------------------------------------------------
                                    
Virtual base class for all materials. Do not instantiate directly, use the
children classes such as 'compound' and 'mixture' for instantiation.

This class provides a general interface for the materials, e.g. for obtaining
energy resolved or effective attenuation coefficients, or a function to 
convert from thickness to optical density and inversely.
                                    
-------------------------------------------------------------------------------

Public methods:
    
    spec_weight():
        get the material specific weight in [g/cm^3]
    att_coeff(e_vals, partial):
        get the energy dependent attenuation coefficients
    transmission(thickness, e_vals, partial):
        get the energy dependent transmission for a given material thickness
    eff_att_coeff(beam, detector, partial, thickness):
        get the effective attenuation coefficient of the material
    eff_mean_free_path(beam, detector):
        get the effective mean free path in the material
    add_porosity(self, porosity):
        create a porous material by adding a void fraction to the current material 
    get_composition(self, frac_type='mole'):
        get the atomic composition as molar or mass fractions    
    get_converter(beam, detector, cdir, mfp_range)
        get an interpolation function to convert from material thickness to
        optical density and inversely
        ---------
        beam = definition of the beam spectrum [PyQNI.beam object]
        detector = definition of the detector [PyQNI.detector object]
        cdir = direction of conversion ('forward' for thickness -> OD,
            'backward' for OD -> thickness)
        mfp_range = thickess values defined as multiple of the mean free path
            used to create the interpolation function. Values outside this
            range will generate an error. Default: from -1 MFP to 10 MFP, in
            steps of 0.1 MFP (the negative range is used to avoid errors when
            noisy data near a transmission of 1.0 resuls in negative thicknesses)

----------------------------------------------------------------------------"""

class material(base):
    
    _spec_weight = None 
    _spec_weight_nat = None 
    
    """--------------------- spec_weight  ----------------------------------"""
    
    def spec_weight(self, **kwargs):
        """
        Get the specific weight of the material, resolving all string
        and function parameters if necessary

        Parameters
        ----------
        **kwargs : Dictionnary
            List of all named parameters.

        Returns
        -------
        Specific weight in [g/cm3].

        """
        
            # Resolve and return the specific weight value
        return self._resolve_value(self._spec_weight, **kwargs)
    
    """----------------------- att_coeff  ----------------------------------"""
    
    def att_coeff(self, e_vals=def_evals, partial='', **kwargs):
        """
        Computes the energy attenuation coefficients of a material as a
        function of neutron energy.

        Parameters
        ----------
        e_vals : numpy array (1D), optional
            List of neutron energies for which the evaluation is done.
            The default is def_evals.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the attenuation coefficient is only considering the elements/
            isotopes in this list. The default is ''.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        e_vals : numpy array
            neutron energy values in [eV].
        ac_vals : numpy array
            attenuation coefficient values in [1/cm].
        """
        
            # virtual method, only implemented in the child classes
        return [],[]
    
    """----------------------- transmission  -------------------------------"""
    
    def transmission(self, thickness, e_vals=def_evals, partial='', **kwargs):
        """
        Computes the transmission through a material of a given thickness
        as a function of neutron energy.

        Parameters
        ----------
        thickness : float
            Material thickness in [cm]
        e_vals : numpy array, optional
            List of neutron energies for which the evaluation is done.
            The default is def_evals.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the transmission value is only considering attenuation from
            the elements/isotopes in this list. The default is ''.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        tuple of 2 numpy arrays:
            - The 1st contains the neutron energies
            - The 2nd contains the transmission values (unitless)
        """    
        
            # Computation of the attenuation coefficients
        _, ac_vals = self.att_coeff(e_vals, partial, **kwargs)
        
            # Computation of the thickness based on attenuation coefficients
        t_vals = np.exp(-thickness*ac_vals)
        
            # Return the values
        return e_vals, t_vals
    
    """----------------------- eff_att_coeff  ------------------------------"""
    
    def eff_att_coeff(self, beam, detector, partial='', thickness=0, **kwargs):
        """
        Get the effective attenuation coefficient of the material for a given
        beam spectrum and a given detector.

        Parameters
        ----------
        beam : PyQNI.beam object
            Definition of the beam spectrum.
        detector : PyQNI.detector object
            Definition of the detector.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the transmission value is only considering attenuation from
            the elements/isotopes in this list. The default is ''.
        thickness : float, optional
            If not 0, compute the attenuation coefficient for a given thickness,
            taking into account beam hardening. If 0, compute the attenuation
            coefficient for an infinitely thin slice.
            The default is 0.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        float
            Effective attenuation coefficient in [cm^-1].
        """
        
            # compute the effective spectrum taking into account the detector
            # capture efficiency
        e_vals, e_weights, int_vals = beam.eff_spectrum(detector, **kwargs)
        
            # If the specified thickness is zero
        if thickness == 0:
                # get the energy dependent attenuation coefficients
            _,ac_vals = self.att_coeff(e_vals, partial=partial, **kwargs)
                
                # compute the average weighted by the effective spectrum
            eff_ac = np.sum(e_weights*int_vals*ac_vals)/np.sum(e_weights*int_vals)
            
            # If the thickness is not zero
        else:
                # compute the energy dependent transmission values
            _, t_vals = self.transmission(thickness, e_vals, partial=partial, **kwargs)
                # compute the effective transmission based on the spectrum
            t_eff = np.sum(e_weights*int_vals*t_vals)/np.sum(e_weights*int_vals)
                # compute the attenuation coefficient corresponding to this transmission
            eff_ac = -np.log(t_eff)/thickness
        
            # return the computed value
        return eff_ac
    
    """--------------------- eff_mean_free_path  ---------------------------"""
    
    def eff_mean_free_path(self, beam, detector, **kwargs):
        """
        Get the effective mean free path of neutrons in the material, for
        a given beam spectrum and a given detector

        Parameters
        ----------
        beam : PyQNI.beam object
            Definition of the beam spectrum.
        detector : PyQNI.detector object
            Definition of the detector.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        float
            mean free path in [cm].

        """
        
            # compute the mean free path as the inverse of the effective
            # attenuation coefficient
        return 1/self.eff_att_coeff(beam, detector, **kwargs)
    
    """----------------------- add_porosity  -------------------------------"""
    
    def add_porosity(self, porosity):
        """
        Create a porous material based on the current material mixed with a
        given volume fraction of void.

        Parameters
        ----------
        porosity : float
            porosity of the newly created material.

        Returns
        -------
        PyQNI.mixture object composed of the current material mixed with
        void.

        """
        
            # create a mixture object and return it
        return mixture([self, (void(), porosity)], frac_type='vol')
    
    """---------------------- get_composition  -----------------------------"""
    
    def get_composition(self, frac_type='mol'):
        """
        Get the atomic composition in the material, defined as mass, volume or
        molar fractions.

        Parameters
        ----------
        frac_type : string
            Type of fraction used ('mol' or 'mass'). Default is 'mol'

        Returns
        -------
        dict
            Dictonary defining the composition (e.g. {'H':0.67, 'O':33})
            for the molar fractions of water.

        """
        
            # virtual function, needs to be defined in the child classes
        return {}
    
    """----------------------- get_converter  ------------------------------"""
    
    def get_converter(self, beam, detector, param='thickness', result='od', 
                      cdir='forward', pmin=None, pmax=None, nbins=10, 
                      interp='cubic', **kwargs):
        """
        Generates a function to convert from thickness or other parameters to
        optical density or transmission, or to perform the equivalent backward
        conversion.

        Parameters
        ----------
        beam : PyQNI.beam object
            Definition of the beam spectrum.
        detector : PyQNI.detector object
            Definition of the detector.
        param : string, optional
            Name of the parameter for which the converter is created.
            The default is 'thickness'.
        result : string, optional
            Result of the conversion. Can be the transmission ('trans') or the
            optical density ('od'). The default is 'od'.
        cdir : string, optional
            Direction of the conversion. Can be 'forward' (the result is
            (computed from the parameter) or 'backward' (the parameter is
            computed from the result). The default is 'forward'.
        pmin : float, optional
            The minimum parameter value for which the converter is valid.
            Mandatory if the parameter is other than 'thickness'. If omitted
            and the parameter is 'thickness', pmin is set to -1*(mean free path).
        pmax : float, optional
            The maximum parameter value for which the converter is valid.
            Mandatory if the parameter is other than 'thickness'. If omitted
            and the parameter is 'thickness', pmax is set to 9*(mean free path).
        nbins : integer, optional
            Number of interpolation intervals. The default is 10.
        interp : string, optional
            Type of interpolation. The default is 'cubic'.
        **kwargs : Dictionnary
            All remaining named parameters.

        Raises
        ------
        ValueError
            If 'result' is other than 'trans' or 'od'
        ValueError
            If the parameter is other than 'thickness' and either cmin or cmax
            are not specified

        Returns
        -------
        cnv : function
            Converter function (can be used on scalar values or on numpy arrays).

        """
        
            # import the necessary modules
        from scipy.interpolate import interp1d
        
            # get a copy of the named parameters list
        _kwargs = kwargs.copy()
        
            # if attenuation coefficients are computed, the thickness is assumed to be zero
        if result == 'ac':
            _kwargs['thickness'] = 0
        
            # if any of the parameter bounds is not specified
        if pmin is None or pmax is None:
            
                # if the parameter is the thickness, use the default bounds
            if param == 'thickness':
                mfp = self.eff_mean_free_path(beam, detector, **kwargs)
                if pmin is None:
                    pmin = -1.0*mfp
                if pmax is None:
                    pmax = 9.0*mfp
                   
                # otherwise raise an error
            else:
                raise ValueError('The parameter bounds need to be specified')
        
            # get the parameter range
        p_vals = np.linspace(pmin, pmax, nbins+1)
        
            # create a buffer for the corresponding AC and OD values
        ac_vals = np.zeros(p_vals.size)
        od_vals = np.zeros(p_vals.size)
        
            # loop for all parameter values
        for i, val in enumerate(p_vals):
                # set the parameter to the current value
            _kwargs[param] = val
            
                # compute the corresponding attenuation coefficient and optical density values
            ac_vals[i] = self.eff_att_coeff(beam, detector, **_kwargs)
            if _kwargs['thickness'] != 0:
                od_vals[i] = _kwargs['thickness']*ac_vals[i]
            
            # forward conversion
        if cdir == 'forward':
            
                # get the interpolation function returning the AC or OD as a function of the parameter
            if result == 'ac':
                _cnv = interp1d(p_vals, ac_vals, kind=interp)
            else:
                _cnv = interp1d(p_vals, od_vals, kind=interp)
            
                # if the desired result is the OD, directly return this function
            if result == 'od' or result == 'ac':
                cnv = _cnv
                
                # otherwise return a function computing the thickness
            elif result == 'trans':
                cnv = lambda x: np.exp(-_cnv(x))
                
                
                # if neither 'trans' nor 'od' is specified, raise an error
            else:
                raise ValueError('the result type must be \'od\', \'trans\' or \'ac\'')
            
            # backward conversion
        elif cdir == 'backward':
            
                # get the interpolation function returning the parameter as a function of the OD or AC:
            if result == 'ac':
                _cnv = interp1d(ac_vals, p_vals, kind=interp)
            else:
                _cnv = interp1d(od_vals, p_vals, kind=interp)
            
                # if the desired result is the OD, directly return this function
            if result == 'od' or result == 'ac':
                cnv = _cnv
                
                # otherwise return a function using the transmission
            elif result == 'trans':
                cnv = lambda x: _cnv(-np.log(x))
                
                # if neither 'trans' nor 'od' is specified, raise an error
            else:
                raise ValueError('the result type must be \'od\' or \'trans\'')
            
            # return the conversion function
        return cnv        
        
"""----------------------------------------------------------------------------

                                    compound
                                    
-------------------------------------------------------------------------------
                                    
This child class from 'material' is used to represent a compound defined by
a single molecule (e.g. H2O). Some additional functions (molar mass,
molecular density, cross sections per molecule) are defined, which have no
meaning in the general material case.
                                    
-------------------------------------------------------------------------------

Constructor:
    
    compound(composition, spec_weight)
    
        composition : dictionnary
            Formula of the compound. The keys represent the elements or isotopes
            and the values to stoichiometry. For example: {'H':2, 'O':1} represents
            H2O. Isotopes are specified with hyphens (e.g. 'Li-6'). If no particular
            isotope is specified, the natural isotopic composition is used.
        spec_weight : float, optional
            Specific weight in [g/cm^3]. The default is None.
        mol_dens : float, optional
            Molecular density in [molecules/cm^3]. The default is None.
        
        If neither the specific weight nor the molecular density are specified, 
        some functions such as 'cross_sections' or 'molar_mass' can still be used.
        The function relying on knowing the material density (e.g. for the
        computation of attenuation coefficients) will fail.
-------------------------------------------------------------------------------

Public methods (other than inherited from the 'material' base class):
    
    molar_mass():
        get the molar mass for the given compound
    mol_dens():
        get the molecular density for the given compound
    cross_sections(e_vals, partial):
        get the energy resolved total or partial neutron cross sections

----------------------------------------------------------------------------"""

class compound(material):
    
    composition = {}
    _mol_dens = None
    
    """----------------- __load_xs_data (private)  -------------------------"""
    
    def __load_xs_data(self, isotope):
        """
        Load the energy resolved cross sections data from their definition
        file. If a special cross section is defined, it is used, otherwise
        the ENDF VIII definition file is used.

        Parameters
        ----------
        isotope : string
            Isotope for which the cross section data is desired.

        Raises
        ------
        ValueError
            if the desired isotope has not cross sections definition file.

        Returns
        -------
        e : numpy array
            neutron energy values in [eV].
        xs : TYPE
            cross section values in [barn].

        """
    
            # import the pyEND6 functions
        import os
        import sys
        PyQNI_dir = os.path.dirname(os.path.abspath(__file__))
        sys.path.append(os.path.join(PyQNI_dir, 'pyENDF6'))
        from ENDF6 import find_section, read_table
        
            # define the file name for the ENDF VIII cross sections
        fname_endf = os.path.join(PyQNI_dir, endf_dir, isotope + '.txt')
        
            # define the file name for the special cross sections
        fname_other = os.path.join(PyQNI_dir, xs_dir, isotope + '.csv')
            
            # if special cross section are defined, they are used
            # (they are prioritized over the ENDF definitions)
        if os.path.exists(fname_other):
            
                # create empty arrays
            e = np.zeros(0)
            xs = np.zeros(0)
    
                # loop through all lines of the file
            with open(fname_other) as f:
                line = f.readline()
                while line:
                    
                        # append the energy and cross section values 
                    fields = line.split(';')
                    e = np.append(e, float(fields[0]))
                    xs = np.append(xs, float(fields[1]))
                    
                        # go to next line
                    line = f.readline()
            
            ### looks like the guy you wrote this never heard of pandas
            ### dataframes ... (todo: rewrite using pandas)
        
            # if no special cross sections are defined, use the ENDF ones
        elif os.path.exists(fname_endf):
    
                # read all the lines in the file
            f = open(fname_endf)
            lines = f.readlines()
            
                # get the data using pyENDF6
                # MF=3 for energy resolved cross sections
                # MT=1 for neutron total cross section
            sec = find_section(lines, MF=3, MT=1)
            e, xs = read_table(sec)
                
            # if neither special nor ENDF cross sections are defined, return an error
        else:
            raise ValueError('Isotope not found: ' + isotope)
            
            # return the cross section values
        return e, xs
    
    """------------------- __load_xs (private)  ----------------------------"""
    
    def __load_xs(self, isotope, e_vals=def_evals):
        """
        Load cross section data and interpolate it for desired energy values.
        The values are obtained by linear interpolation from the original data.

        Parameters
        ----------
        isotope : string
            Isotope for which the data is required.
        e_vals : numpy array, optional
            The energy values for which the cross sections are required.
            The default is def_evals.

        Returns
        -------
        e_vals : numpy array
            neutron energy values in [eV].
        xs_vals : TYPE
            cross section values in [barn].

        """    
        from scipy.interpolate import interp1d
        
            # get the original data
        e, xs = self.__load_xs_data(isotope)
    
            # create the linear interpolation function
        intf = interp1d(e, xs)
    
            # get the interpolated cross sections
        xs_vals = intf(e_vals)
        
            # return the values
        return e_vals, xs_vals
    
    """----------------------- spec_weight  --------------------------------"""
    
    def spec_weight(self, **kwargs):
        """
        Return the specific weight of the compound (either directly or from the
        molecular density).

        Parameters
        ----------
        **kwargs : Dictionnary
            List of all named parameters.

        Returns
        -------
        sw_val: float
            specific weight in [g/cm^3]

        """
        Na = 6.02e23 # Avogadro number
        
            # resolve the value of the specific weight
        sw_val = self._resolve_value(self._spec_weight, **kwargs)
        
            # if not defined, compute from the molecular density
        if sw_val is None:                        
            md_val = self._resolve_value(self._mol_dens, **kwargs)
            
                # if the molecular density is not defined, compute it from the
                # natural specific weight
            if md_val is None:
                nsw_val = self._resolve_value(self._nat_spec_weight, **kwargs)
                md_val = nsw_val*Na/self.molar_mass(nat_mass=True, **kwargs)
                
            sw_val = self.molar_mass(**kwargs)*md_val/Na
            
            # return the computed value
        return sw_val
    
    """------------------------ molar_mass  --------------------------------"""
    
    def molar_mass(self, nat_mass=False, **kwargs):
        """
        Return the molar mass of the compound.

        Parameters
        ----------
        **kwargs : Dictionnary
            List of all named parameters.

        Returns
        -------
        mm_tot : float
            Molar mass in [g/mol].

        """    
        
            # Initial value of 0
        mm_tot = 0.0
        
            # for each element/isotope in the composition
        for element in self.composition.keys():
            
                # get the stoichiometry of the current element/isotope
            st = self._resolve_value(self.composition[element], **kwargs)
            
                # get the atomic mass of the element and add it to the molar mass
                # (multiplied by the stoichiometry)
            mm_tot += st*self._atom_mass(element, nat_mass)
                
            # return the total molar mass
        return mm_tot
    
    """------------------------ mol_dens  --------------------------------"""
    
    def mol_dens(self, **kwargs):
        """
        Return the molecular density of the compound.

        Parameters
        ----------
        **kwargs : Dictionnary
            List of all named parameters.

        Returns
        -------
        md : float
            Molecular density in [molecules/cm^3].

        """
        
            # compute the molecular density from the specific weight
        md = Na*self.spec_weight(**kwargs)/self.molar_mass(**kwargs)
        
            # return the computed value
        return md
    
    """---------------------- cross_sections  ------------------------------"""
    
    def cross_sections(self, e_vals=def_evals, partial='', **kwargs):
        """
        Return the partial or total energy dependent cross sections of the 
        compound.

        Parameters
        ----------
        e_vals : numpy array (1D), optional
            List of neutron energies for which the evaluation is done.
            The default is def_evals.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the attenuation coefficient is only considering the elements/
            isotopes in this list. The default is ''.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        e_vals : numpy array
            neutron energy values in [eV].
        xs_vals : numpy array
            cross section values in [barn].

        """
        
        xs_tot = np.zeros(e_vals.size)
        
        for key in self.composition.keys():
            
            frac = self._resolve_value(self.composition[key], **kwargs)
            
            if key in el_list.keys():
                if partial == '' or key in partial.split(','):
                    _, el_xs = compound(el_list[key]).cross_sections(e_vals, **kwargs)
                else:
                    _, el_xs = compound(el_list[key]).cross_sections(e_vals, partial=partial, **kwargs)
            else:
                if partial == '' or key in partial.split(','):
                    _, el_xs = self.__load_xs(key, e_vals)
                else:
                    el_xs = np.zeros(e_vals.size)
                
            xs_tot += frac*el_xs
                
        return e_vals, xs_tot
    
    """--------------------- eff_cross_section  ----------------------------"""
    
    def eff_cross_section(self, beam, detector, partial='', **kwargs):
        """
        Get the effective cross section of the compound, for a given
        beam spectrum and detector.

        Parameters
        ----------
        beam : PyQNI.beam object
            Definition of the beam spectrum.
        detector : PyQNI.detector object
            Definition of the detector.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the transmission value is only considering attenuation from
            the elements/isotopes in this list. The default is ''.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        eff_xs : float
            effective cross section in [barn].

        """
        
            # get the effective beam spectrum (taking into account the
            # detector efficiency)
        e_vals, e_weights, int_vals = beam.eff_spectrum(detector, **kwargs)
        
            # get the energy dependent cross sections
        _,xs_vals = self.cross_sections(e_vals, partial=partial, **kwargs)
        
            # compute the effective cross section as a weighted average
        eff_xs = np.sum(e_weights*int_vals*xs_vals)/np.sum(e_weights*int_vals)
        
            # return the computed value
        return eff_xs
    
    """------------------------- att_coeff  --------------------------------"""
    
    def att_coeff(self, e_vals=def_evals, partial='', **kwargs):
        """
        Computes the energy attenuation coefficients as a function of neutron
        energy.

        Parameters
        ----------
        e_vals : numpy array, optional
            List of neutron energies for which the evaluation is done.
            The default is def_evals.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the attenuation coefficient is only considering the elements/
            isotopes in this list. The default is ''.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        e_vals : numpy array
            neutron energy values in [eV].
        ac_vals : numpy array
            attenuation coefficient values in [1/cm].
        """
            
            # get the molecular density in [1/cm^3]
        md = self.mol_dens(**kwargs)
            
            # get the neutron cross sections in [barns] (= 1e-24 [cm^2])
        _, xs_vals = self.cross_sections(e_vals, partial=partial, **kwargs)
        
            # compute the attenuation coefficients in [1/cm]
        ac_vals = xs_vals*1e-24*md
        
        return e_vals, ac_vals
    
    """---------------------- get_composition  -----------------------------"""
    
    def get_composition(self, frac_type='mol'):
            
        atoms_list = self.composition.keys()
        atoms_counts = np.array([self.composition[x] for x in atoms_list])
        
        if frac_type == 'mol':
            frac_vals = atoms_counts/np.sum(atoms_counts)
        
        elif frac_type == 'mass':
            
            atoms_masses = [self._atom_mass(x) for x in atoms_list]
                    
            frac_vals = atoms_counts*atoms_masses/np.sum(atoms_counts*atoms_masses)
        
        else:
            raise ValueError('The fraction type must be "mol" or "mass"')
        
        comp = {}
        
        for i, atom in enumerate(atoms_list):
            if atom in comp.keys():
                comp[atom] = comp[atom] + frac_vals[i]
            else:
                comp[atom] = frac_vals[i]
                
        return comp
    
    """------------------------ constructor  -------------------------------"""
    
    def __init__(self, composition, spec_weight=None, mol_dens=None, nat_spec_weight=None):
        """
        Constructor for the 'compound' class.

        Parameters
        ----------
        composition : dictionnary
            Formula of the compound. The keys represent the elements or isotopes
            and the values to stoichiometry. For example: {'H':2, 'O':1} represents
            H2O. Isotopes are specified with hyphens (e.g. 'Li-6'). If no particular
            isotope is specified, the natural isotopic composition is used.
        spec_weight : float, optional
            Specific weight in [g/cm^3]. The default is None.
        mol_dens : float, optional
            Molecular density in [molecules/cm^3]. The default is None.

        Returns
        -------
        None.

        """
        
            # store the parameters as class data
        self._spec_weight = spec_weight
        self._nat_spec_weight = nat_spec_weight
        self._mol_dens = mol_dens
        self.composition = composition

"""----------------------------------------------------------------------------

                                    void
                                    
-------------------------------------------------------------------------------
                                    
This child class from 'material' is used to represent an empty space. It can be
used in mixtures to create porous materials.
                                    
-------------------------------------------------------------------------------

Constructor:
    void() - no parameters are necessary
    
----------------------------------------------------------------------------"""
        
class void(material):
    
    """------------------------- att_coeff  --------------------------------"""
    
    def att_coeff(self, e_vals=def_evals, partial='', **kwargs):
        """
        Computes the energy attenuation coefficients as a function of neutron
        energy.

        Parameters
        ----------
        e_vals : numpy array, optional
            List of neutron energies for which the evaluation is done.
            The default is def_evals.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the attenuation coefficient is only considering the elements/
            isotopes in this list. The default is ''.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        e_vals : numpy array
            neutron energy values in [eV].
        ac_vals : numpy array
            attenuation coefficient values in [1/cm].
        """
        
            # return zero values for the attenuation coefficients
        return e_vals, np.zeros(e_vals.size)
    
    """------------------------ spec_weight  -------------------------------"""
    
    def spec_weight(self, **kwargs):
        """
        Return the specific weight.

        Parameters
        ----------
        **kwargs : Dictionnary
            List of all named parameters.

        Returns
        -------
        sw_val: float
            specific weight in [g/cm^3]

        """
            # return 0 for the specific weight
        return 0.0

"""----------------------------------------------------------------------------

                                    mixture
                                    
-------------------------------------------------------------------------------
                                    
This child class from 'material' is used to represent a mixture of materials
(either compounds, void or other mixtures). The fractions of the different
components of the mixture can be defined as mass fractions, volume fractions
or mole fractions.
                                    
-------------------------------------------------------------------------------

Constructor:
    
    mixture(components, spec_weight, frac_type)
    
        components : array
            the materials and their fractions. The format
            is an array where each element is either just a material or a
            tuple made of a material and its fraction. If the fractions are
            defined for all materials but one, the missing fraction is
            calculated assuming a total of 1.0 for all fractions. If no
            fractions are defined, all materials are assumed to have the
            same fraction.
        spec_weight: float
            the specific weight of the resulting mixture. If not
            specified, the resulting specific weight is calculated assuming
            that the volume occupied by all materials is the sum of the
            volumes of the individual materials.
        frac_type: string
            the type of fractions defined in 'components', can be
            either 'mass' (the default), 'vol', or 'mole'. If one of the
            materials is 'void', only volume fractions are allowed. Mole
            fractions are only allowed when all components are of type
            'compound'.    

-------------------------------------------------------------------------------

Public methods:
    No public methods (other than the ones inherited from the 'material' class)

----------------------------------------------------------------------------"""

class mixture(material):
    
    _comp_list = []
    _frac_list = []
    _frac_type = ''
    _spec_weight = None
    
    """------------------------ spec_weight  -------------------------------"""
    
    def spec_weight(self, **kwargs):
        """
        Return the specific weight.

        Parameters
        ----------
        **kwargs : Dictionnary
            List of all named parameters.

        Returns
        -------
        sw_val: float
            specific weight in [g/cm^3]

        """
        
            # if the specific weight is not defined, compute it from the
            # components
        if self._spec_weight is None:
            
                # get the volume fractions
            vf_vals = self._get_vol_fractions(**kwargs)
            
                # get the list of specific weights
            sw_vals = np.array([comp.spec_weight(**kwargs) for comp in self._comp_list])
            
                # compute the resulting specific weight
            sw_val = sum(vf_vals*sw_vals)
            
            # if the specific weight is defined, resolve its value
        else:
            sw_val = self._resolve_value(self._spec_weight, **kwargs)
            
            # return the computed specific weight
        return sw_val
    
    """------------------------- att_coeff  --------------------------------"""
    
    def att_coeff(self, e_vals=def_evals, partial='', **kwargs):
        """
        Computes the energy attenuation coefficients as a function of neutron
        energy.

        Parameters
        ----------
        e_vals : numpy array, optional
            List of neutron energies for which the evaluation is done.
            The default is def_evals.
        partial : string, optional
            List of comma separated elements of isotopes. If not empty,
            the attenuation coefficient is only considering the elements/
            isotopes in this list. The default is ''.
        **kwargs : Dictionnary
            All remaining named parameters.

        Returns
        -------
        e_vals : numpy array
            neutron energy values in [eV].
        ac_vals : numpy array
            attenuation coefficient values in [1/cm].
        """
        
            # buffer for the results
        ac_vals = np.zeros(e_vals.size)
        
            # get the volume fractions
        vf_vals = self._get_vol_fractions(**kwargs)
        
            # for each component, get the attenuation coefficients and add
            # them to the buffer (weighted by the volume fractions)
        for i, cmp in enumerate(self._comp_list):
            _, ac = cmp.att_coeff(e_vals, partial=partial, **kwargs)
            ac_vals += vf_vals[i]*ac
            
            # return the results
        return e_vals, ac_vals
    
    """------------------------- att_coeff  --------------------------------"""
    
    def __unpack_components(self, components):
        """
        Unpack the components arry into two arrays (components and fractions)

        Parameters
        ----------
        components : array
            array where each element is either just a material or a tuple made
            of a material and its fraction

        Returns
        -------
        comp_list : array of PyQNI.material objects
            array containing the components definitions.
        frac_list : array
            Array containing the fractions (None if undefined).

        """
            # create an empty list
        comp_list = []
        frac_list = []
        
            # iterate for all component definitions
        for element in components:
            
                # if the definition is a tuple, the firt element is the component
                # and the second is the fraction
            if isinstance(element, tuple):
                comp = element[0]
                frac = element[1]
                
                # if the definition contains only the component, the fraction
                # is marked as undefined
            else:
                comp = element
                frac = None
                
                # append the values to the lists
            comp_list.append(comp)
            frac_list.append(frac)
            
            # return the computed list
        return comp_list, frac_list
    
    """---------------------- get_composition  -----------------------------"""
    
    def get_composition(self, frac_type='mol'):
        
        comp_mf_vals = self._get_mass_fractions()
        
        atoms_list = np.zeros(0)
        atoms_mf = np.zeros(0)
        
        for i, comp in enumerate(self._comp_list):
            
            cmp = comp.get_composition(frac_type='mass')
            
            for atom, mf in cmp.items():
                
                atoms_list = np.append(atoms_list, atom)
                atoms_mf = np.append(atoms_mf, mf*comp_mf_vals[i])
                
        if frac_type == 'mass':
            
            atoms_fracs = atoms_mf
            
        elif frac_type == 'mol':
            
            atoms_masses = np.array([self._atom_mass(x) for x in atoms_list])
            
            atoms_fracs = atoms_mf/atoms_masses/np.sum(atoms_mf/atoms_masses)

        else:
            raise ValueError('The fraction type must be "mol" or "mass"')   
            
        comp = {}
        
        for i, atom in enumerate(atoms_list):
            
            if atom in comp.keys():
                comp[atom] = comp[atom] + atoms_fracs[i]
                
            else:
                comp[atom] = atoms_fracs[i]
        
        return comp
    
    """--------------- __resolve_fractions (private)  ----------------------"""
    
    def __resolve_fractions(self, **kwargs):
        """
        Resolve the specified fractions, denepnding on which are defined.
        - If all fractions are defined, normalize so that the sum is 1.0
        - If one single fraction is undefined, assume is it the complement to 
          1.0 of the sum of the other fractions
        - If no fraction is defined, assume they are all equal
        - Otherwise, raise an error

        Parameters
        ----------
        **kwargs : Dictionnary
            All named parameters.

        Raises
        ------
        ValueError
            If the fractions definition does not correspond to one of the 3
            cases defined above.
        ValueError
            If one fraction is undefined and the sum of the other is more than 1.0

        Returns
        -------
        f_vals : numppy array
            Values of the resolved fractions.

        """
        
            # get the number of components and create an output array for
            # the fractions
        ncomp = len(self._comp_list)
        f_vals = np.zeros(ncomp)
        
            # resolve the value of all fractions
        for i, comp in enumerate(self._comp_list):
            f_vals[i] = self._resolve_value(self._frac_list[i], **kwargs)
            
            # get the list of undefined fractions
        undef_fracs = np.where(np.isnan(f_vals))[0]
        
            # if there is one single undefined fraction, the complement to 1.0 is used
        if undef_fracs.size == 1:
            
                # get the sum of all defined fractions
            f_tot = sum(f_vals[np.where(np.invert(np.isnan(f_vals)))[0]])
            
                # if less than 1.0, get the complement to 1.0
            if f_tot <= 1.0:
                f_vals[undef_fracs] = 1.0 - f_tot
                
                # otherwise raise an error
            else:
                raise ValueError('When a single undefined fraction is used, the sum of the other fractions should be less than 1.0')
                
                
            # if none of the fractions is defined, use equal fractions
        elif undef_fracs.size == f_vals.size:
            f_vals[:] = 1.0/ncomp
            
            # if all fractions are defined, normalize so that the sum is 1.0
        elif undef_fracs.size == 0:
            f_vals = f_vals/sum(f_vals)
            
            # in all other cases, raise an error
        else:            
            raise ValueError('Either no fractions, all fractions, or all fractions but one have to be defined')
        
            # return the computed fractions
        return f_vals   
    
    """--------------- _get_mass_fractions (protected)  --------------------"""  
    
    def _get_mass_fractions(self, **kwargs):
        
            # get the volume fractions
        vf_vals = self._get_vol_fractions(**kwargs)
        
            # get the list of specific weights
        sw_vals = np.array([comp.spec_weight(**kwargs) for comp in self._comp_list])
            
            # convert to mass fractions, normalizing so that the sum is 1.0
        mf_vals = vf_vals*sw_vals/sum(vf_vals*sw_vals)
        
        return mf_vals
    
    """--------------- _get_vol_fractions (protected)  ---------------------"""
    
    def _get_vol_fractions(self, **kwargs):
        """
        Convert the fractions to volume fractions.

        Parameters
        ----------
        **kwargs : Dictionnary
            All named parameters.

        Raises
        ------
        ValueError
            If the definition is in molar fractions and on of the components is
            not a compound
        ValueError
            If the fraction type is other than 'vol', 'mass' or 'mole'

        Returns
        -------
        vf_vals : numpy array
            Computed volume fractions.

        """
        
            # get the resolved fractions
        f_vals = self.__resolve_fractions(**kwargs)
        
            # resolve the specific weight value
        spec_weight = self._resolve_value(self._spec_weight, **kwargs)
        
            # get the list of specific weights
        sw_vals = np.array([comp.spec_weight(**kwargs) for comp in self._comp_list])
        
            # if the fraction type is volume, keep the values 
        if self._frac_type == 'vol':
            vf_vals = f_vals
            
            # if the fraction type is mass, convert to volume fractions, 
            # normalizing so that the sum is 1.0
        elif self._frac_type == 'mass':
            vf_vals = f_vals/sw_vals/sum(f_vals/sw_vals)
            
            # if the fraction type is mole
        elif self._frac_type == 'mole':
        
            md_vals = np.zeros(len(self._comp_list))
                # iterate for all components
            for i, comp in enumerate(self._comp_list):
                
                    # if it is a compound, get its molecular density
               if isinstance(comp, compound):
                   md_vals[i] = comp.mol_dens(**kwargs)
                   
                   # otherwise raise an error
               else:
                   raise ValueError('Mole fractions not allowed for a mixture of mixtures')
            
                # convert the mole fractions to volume fractions, normalizing
                # so that the sum is 1.0
            vf_vals = f_vals/md_vals/sum(f_vals/md_vals)
            
            # if the fraction type is not from the allowed list, raise an error
        else:
            raise ValueError('Allowed fraction types are: ' + str(frac_type_list))
            
            # if a resulting specific weight is defined, scale the volume
            # fractions correspondingly
        if spec_weight is not None:
            vf_vals *= spec_weight/np.sum(vf_vals*sw_vals)
        
            # return the computed values
        return vf_vals           
    
    """------------------------ constructor  -------------------------------"""
    
    def __init__(self, components, spec_weight=None, frac_type='mass'):
        """
        Contructor for the 'mixture' class

        Parameters
        ----------
        components : array
            the materials and their fractions. The format
            is an array where each element is either just a material or a
            tuple made of a material and its fraction. If the fractions are
            defined for all materials but one, the missing fraction is
            calculated assuming a total of 1.0 for all fractions. If no
            fractions are defined, all materials are assumed to have the
            same fraction.
        spec_weight: float
            the specific weight of the resulting mixture. If not
            specified, the resulting specific weight is calculated assuming
            that the volume occupied by all materials is the sum of the
            volumes of the individual materials.
        frac_type: string, optional
            the type of fractions defined in 'components', can be
            either 'mass', 'vol', or 'mole'. If one of the materials is 'void',
            only volume fractions are allowed. Mole fractions are only allowed
            when all components are of type 'compound'. The default is 'mass'

        """
        
            # update the class members base on the specified parameters
        self._comp_list, self._frac_list = self.__unpack_components(components)
        self._frac_type = frac_type
        self._spec_weight = spec_weight
        

        
"""----------------------------------------------------------------------------

                                    solution
                                    
-------------------------------------------------------------------------------
                                    
This child class from 'mixture' is used to represent a solution as a particular
type of mixture defined by a solvent and one or more solutes. The amount
of solute(s) is specified by giving their concentrations in [mol/L] (mole of
solute per liter of solvent).
                                    
-------------------------------------------------------------------------------

Constructor:
    
    solution(self, solvent, solute, spec_weight=None)
    
        solvent : PyQNI.material
            Solvent used for the solution. Can be a compound or a mixture.
        solute : tuple (PyQNI.compound, concentration) or array of tuples
            Solute(s) used for the solution.
        spec_weight : float, optional
            Specific weight of the resulting solution. The default is None,
            in which case the specific weight is calculated assuming that the
            resulting volume is the sum of the solvent and solute(s) volumes.  

-------------------------------------------------------------------------------

Public methods:
    No public methods (other than the ones inherited from the 'material' class)

----------------------------------------------------------------------------"""

class solution(mixture):    
    
    _solute = None
    _solvent = None
    _spec_weight = None
    _full_dilution = None
    
    """--------------- _get_vol_fractions (protected)  ---------------------"""
    
    def _get_vol_fractions(self, **kwargs):
        """
        Convert the solute concentrations to volume fractions of solute and solvent.

        Parameters
        ----------
        **kwargs : Dictionnary
            All named parameters.

        Returns
        -------
        vf_vals : numpy array
            Computed volume fractions.

        """
        
            # get the solute concentrations in mol/L
        concentrations = [self._resolve_value(x[1]) for x in self._solute]
        
            # empty buffer for the solute volumes
        solute_vols = np.zeros(len(concentrations))
        
            # loop for all solutes
        for i, c in enumerate(concentrations):
            
                # get the molar volume of the solute (in L/mol)
            mv = 1/(1000*self._comp_list[i+1].mol_dens()/Na)
            
                # get the volume corresponding to the concentration (for 1L of solvent)
            solute_vols[i] = c*mv
            
            # create a list of volumes (with first 1L for the solvent)
        v_list = np.append(1.0, solute_vols)
        
            # if "full dilution" is set, keep these volume fractions
            # so that the solven volume fraction is 1.0
        if self._full_dilution:
            vf_vals = v_list
            
            # otherwise normalize the volumes to have a sum of 1.0
        else:
            vf_vals = v_list/np.sum(v_list)
        
        if self._spec_weight is not None:
            
                # get the list of specific weights
            sw_vals = np.array([comp.spec_weight(**kwargs) for comp in self._comp_list])
            
                # adjust the volume fractions to match the desired specific weight
            vf_vals = vf_vals*self._spec_weight/sum(vf_vals*sw_vals)
        
            # return the obtained volume fractions
        return vf_vals
            
    
    """------------------------ constructor  -------------------------------"""
    
    def __init__(self, solvent, solute, spec_weight=None, full_dilution=False):
        """
        Constructor for the 'solution' class

        Parameters
        ----------
        solvent : PyQNI.material
            Solvent used for the solution. Can be a compound or a mixture.
        solute : tuple (PyQNI.compound, concentration) or array of tuples
            Solute(s) used for the solution.
        spec_weight : float, optional
            Specific weight of the resulting solution. The default is None,
            in which case the specific weight is calculated assuming that the
            resulting volume is the sum of the solvent and solute(s) volumes.

        """
        
        self._solute = [solute] if isinstance(solute, tuple) else solute
        self._solvent = solvent
        self._spec_weight = spec_weight
        self._full_dilution = full_dilution
        self._comp_list = [solvent] + [x[0] for x in self._solute]
        
            # if "full dilution" is activated, the specific weight of the
            # solutes is not relevant. They are all set to 1.0 for the case
            # where they are not specified
        if self._full_dilution:
            for i in range(1,len(self._comp_list)):
                self._comp_list[i]._spec_weight = 1.0
        
"""----------------------------------------------------------------------------

                                    scintillator
                                    
-------------------------------------------------------------------------------
"""

class scintillator(mixture):
    
    _capt_element = ''
           
    #--------------------------------------
    
    def att_coeff(self, e_vals=def_evals, partial='', **kwargs):
        
        if partial is None:
            return mixture.att_coeff(self, e_vals, partial=self._capt_element, **kwargs)
        else:
            return mixture.att_coeff(self, e_vals, partial, **kwargs)
           
    #--------------------------------------
    
        # constructor based on composition, specific weight, type of fractions
        # and capture element
    def __init__(self, components, spec_weight=None, frac_type='mass',
                 capt_element=''):
        
        self._capt_element = capt_element
        mixture.__init__(self, components, spec_weight, frac_type)
        
        
"""----------------------------------------------------------------------------

                                    detector
                                    
-------------------------------------------------------------------------------
                                    
Class defining the detector in order to compute the capture efficiency as a
function of the neutron energy. The detector is characterized by the
scintillator material, the element or isotope within this scintillator
responsible for the neutron capture, and the thickness of the material.
                                    
-------------------------------------------------------------------------------

Constructor:
    
    detector(sc_material, capt_element, thickness)
    
        sc_material = material used for the scintillator screen (or for the
                neutron capturing layer if the detector is not scintillator
                based). Type: PyQNI.compund or PyQNI.mixture
        capt_element = the element or isotope responsible for the neutron
                capture. Type: string, e.g. 'Gd' or 'Li-6'
        thickness = thickness in [cm] of the scintillator screen or of the neutron
                capturing layer

-------------------------------------------------------------------------------

Public methods:
    
    capt_eff(e_vals):
        get the capture efficiency as a function of neutron energy
        ---------
        e_vals = energy values [eV] for which to return the att. coeffs.
                 (default: from 0.1 meV to 10 eV, 100 pts/decade)
        ---------
        return value = (energy values [eV], capture efficiencies [-])
                 
----------------------------------------------------------------------------"""
        
class detector(base):
    
    _sc_material = void()
    _thickness = 0.0
    
    #--------------------------------------
    
    def capt_eff(self, e_vals=def_evals, **kwargs):
        
        thickness = self._resolve_value(self._thickness, **kwargs)
        
        _, ac_vals = self._sc_material.att_coeff(e_vals, partial=None, **kwargs)
        
        ce_vals = 1 - np.exp(-ac_vals*thickness)
        
        return e_vals, ce_vals
    
    #--------------------------------------
    
    def __init__(self, sc_material, thickness):
    
        self._sc_material = sc_material
        self._thickness = thickness
        

        
"""----------------------------------------------------------------------------

                                    beam
                                    
-------------------------------------------------------------------------------
                                    
Class representing the beam spectrum. The spectrum data must be defined in a CSV
file with two columns.
                                    
-------------------------------------------------------------------------------

Constructor:
    
    beam(beam_line)
    
        beam_line = name of the beam line (string)

-------------------------------------------------------------------------------

Public methods:
    
    int_spectrum():
        get the cumulated spectrum (integral of the spectrum intensity)
        ---------
        return value: (energy values [eV], integral values [-])
        
    spectrum(norm_type):
        get the beam spectrum 
        ---------
        norm_type = definition of the normalization for the intensity units:
            'log' (default): intensity defined in count/delta(ln E)
            'energy': intensity defined in counts/delta(E)
            'wavelength': intensity defined in counts/delta(lambda)
        ---------
        return value: (energy values [eV], interval sizes [*], intensities [-])
        
    eff_spectrum(norm_type):
        get the effective beam spectrum, taking into account the detector
        efficiency
        ---------
        detector = detector used to capture the neutrons (PyQNI.detector object)
        norm_type = definition of the normalization for the intensity units:
            'log' (default): intensity defined in count/delta(ln E)
            'energy': intensity defined in counts/delta(E)
            'wavelength': intensity defined in counts/delta(lambda)
        ---------
        return value: (energy values [eV], interval sizes [*], intensities [-])
        
        *the interval sizes unit depends on the type of normalization 
                 
----------------------------------------------------------------------------"""
        
class beam(base):
    
    e_vals = np.array([])
    c_vals = np.array([])
    _name = ''
    
    #--------------------------------------
    
    def int_spectrum(self):
    
        l = self.c_vals.size
        
        i_vals = np.zeros(l)
        
        int_val = 0.0
        
        for i in np.arange(0, l):
            
            i_vals[i] = int_val
            
            int_val += self.c_vals[i]
        
        return self.e_vals, i_vals
    
    #--------------------------------------
    
    def spectrum(self, norm_type='log'):
    
            # get the number of lines in the spectrum definition
        l = self.c_vals.size
        
            # compute the size of the bins
            # 'energy' normalization: difference of two succesive energies
        if norm_type == 'energy':
            e_weights = self.e_vals[1:l] - self.e_vals[0:l-1]
            
            # 'wavelength' normalization: difference of two succesive wavelengths
        elif norm_type == 'wavelength':
            lambda_vals = (energy_1A/self.e_vals)**0.5 # conversion from energy to wavelength
            e_weights = lambda_vals[0:l-1] - lambda_vals[1:l]
            
            # 'log' normalization: natural logarithm of the ratio of energies
        else:
            e_weights = np.log(self.e_vals[1:l]/self.e_vals[0:l-1])
            
            # get the 'average' energy values, corresponding to the middle of
            # the bins
        e_vals_avg = (self.e_vals[1:l]+self.e_vals[0:l-1])/2
        
            # get the intensity values, computed as the counts for a given bin
            # devided by the size of that bin
        int_vals = self.c_vals[0:l-1]/e_weights
            
            # return the results
        return e_vals_avg, e_weights, int_vals
    
    #--------------------------------------
    
    def eff_spectrum(self, detector, norm_type='log', **kwargs):
        
            # get the normalized spectrum
        e_vals, e_weights, int_vals = self.spectrum(norm_type)
        
            # get the capture efficiencies
        _, ce = detector.capt_eff(e_vals, **kwargs)
            
            # return the effective spectrum as the product of the spectrum
            # and the capture efficiencies
        return e_vals, e_weights, int_vals*ce
    
    #--------------------------------------
    
    def name(self):
        
            # return the beam line name        
        return self._name
    
    #--------------------------------------
    
    def __init__(self, beam_line, e_range=(1e-3,1e0)):
    
            # import necessary modules
        import os
        
        PyQNI_dir = os.path.dirname(os.path.abspath(__file__))
        
            # update the beam line name
        self._name = beam_line
        
            # get the spectrum file name
        fname = os.path.join(PyQNI_dir, spectra_dir, beam_line+'.csv')
        
            # read all the contents
        f = open(fname)
        lines = f.readlines()
        
            # get the number of lines
        l = len(lines)
        
            # create the results buffers
        e_vals = np.zeros(l-1)
        c_vals = np.zeros(l-1)
        
            # loop for all lines except the first one
            # (assuming it contains headers)
        for i in np.arange(0, l-1):
            
                # get the energy and counts from the first and
                # second column, respectively
            e_str = (lines[i+1].strip().split(';'))[0]
            c_str = (lines[i+1].strip().split(';'))[1]
            
                # if the corresponding strings are not empy, update the value
                # in the buffer
            if e_str != '':
                e_vals[i] = float(e_str)
            if c_str != '':
                c_vals[i] = float(c_str)
                
                # get the indices of the values withing the selected energy range
        keep = np.where(np.logical_and(e_vals >= e_range[0], e_vals <= e_range[1]))
                
                # store the read values in the corresponding object members
        self.e_vals = e_vals[keep]
        self.c_vals = c_vals[keep]
    
    #--------------------------------------
        
            
        