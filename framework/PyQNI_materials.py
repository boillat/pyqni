# -*- coding: utf-8 -*-
"""
Created on Tue Jun 29 20:56:52 2021

@author: boillat
"""

from PyQNI import compound

water = compound(composition={'H-1-H2O-EXS':2.0, 'O':1.0}, spec_weight=0.997) # specific weight at 25°C, from https://hbcp.chemnetbase.com/faces/documents/15_20/15_20_0001.xhtml